import classes from './header.module.scss'
import Switcher from "./components/swicher/Switcher.jsx";
import Search from "./components/search/Search.jsx";
import CurrentLocation from "./components/curentLocation/CurrentLocation.jsx";

const Header = () => {
    return (
        <header className={classes.header}>
            <Switcher/>
            <Search/>
            <CurrentLocation/>
        </header>
    );
};
export default Header;
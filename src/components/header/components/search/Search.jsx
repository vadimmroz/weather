import classes from './search.module.scss'
import {useState} from "react";
import axios from "axios";
import {useDispatch} from "react-redux";
import {setCity} from "../../../../store/slices/citySlice.js";
import Variants from "./components/variants/Variants.jsx";
const Search = () => {
    const [search, setSearch] = useState('')
    const dispatch = useDispatch()
    const [preloader, setPreloader] = useState(false)
    const handleInput = (e) => {
        setSearch(e.target.value)
    }
    const fetchSearch = async ()=>{
        setPreloader(true)
        let data = await fetchData()
        if (data) {
            const {lat, lon} = data.data.coord
            dispatch(setCity({latitude: lat, longitude: lon}))
            setSearch('')
            setPreloader(false)
        }
    }
    const handleEnter =  (e) => {
        if (e.code === 'Enter') {
            fetchSearch()
        }
    }

    const fetchData = async () => {
        let refactorSearch = search.replace(" ", "+")
        return await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${refactorSearch}&appid=4d381ca2ba2085babc8c19011e7179c1`)
    }

    return (
        <div className={classes.search} onKeyDown={handleEnter}>
            <img onClick={fetchSearch}
                src="https://i.ibb.co/1ZVgZqC/search-1.png"
                alt=""/>
            <input value={search} onChange={handleInput} type="text"
                   placeholder='Search for your preffered city...'/>
            {preloader && <div className={classes.preloader}></div>}
            {/*{search && <Variants value={search}/>}*/}
        </div>
    );
};

export default Search;
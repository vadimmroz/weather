import classes from './variants.module.scss'
import {useEffect, useState} from "react";
import axios from "axios";
import {useQuery} from "react-query";

const fetchSearch = async (value)=>{
    return await axios(`https://data-api.oxilor.com/rest/search-regions?searchTerm=${value}&population=0&first=100&type=city&key=j-PZLmJFxF4NdkK92MLsPiumcWiWlw`)
}

const Variants = ({value}) => {
    const {data, isSuccess} = useQuery(['list', value], ()=>fetchSearch(value))
    const  [list, setList] = useState(null)
    useEffect(()=>{
        if(isSuccess) {
            let a = data.data.sort((a, b) => Number(a.id) > Number(b.id) ? 1 : -1)
            console.log(a)
            setList(data)
        }
    }, [data])
    useEffect(() => {
        console.log(list)
    }, [list]);
    return (
        <div className={classes.variants}>
            {list && list.data.map((e,index)=>{
                // console.log(e)
                return(
                    <div key={index}>
                        <p></p>
                    </div>
                )
            })}
        </div>
    );
};

export default Variants;
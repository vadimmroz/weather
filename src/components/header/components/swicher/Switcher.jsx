import {useTheme} from "../../../../hooks/useTheme.js";
import classes from './switcher.module.scss'
import classNames from "classnames";

const Switcher = () => {
    const {theme, setTheme} = useTheme()
    const handleTheme = ()=>{
        setTheme(theme==='light' ? 'dark' : 'light')
    }

    return (
        <div className={classes.switcher} onClick={handleTheme}>
            <div className={theme === 'light' ? classes.switcherCircle : classNames(classes.switcherCircle, classes.switcherDark)}/>
        </div>
    );
};

export default Switcher;
import {useDispatch, useSelector} from "react-redux";
import {setCity} from "../../../../store/slices/citySlice.js";
import {useEffect} from "react";
import classes from './currentLocation.module.scss'

const CurrentLocation = () => {
    const dispatch = useDispatch()
    const data = useSelector(state => state.data.data)
    const handleButton = () => {
        navigator.geolocation.getCurrentPosition((position) => {
            const {latitude, longitude} = position.coords
            dispatch(setCity({latitude, longitude}))
        })
    }
    useEffect(handleButton, [dispatch])

    return (
        <button className={classes.currentLocation}>
            <img onClick={handleButton}

                 src="https://i.ibb.co/XLtM4dk/current-location-icon.png"
                 alt=""/>
            <span>{data && data.name}</span>
        </button>
    );
};

export default CurrentLocation;
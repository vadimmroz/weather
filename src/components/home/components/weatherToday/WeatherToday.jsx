import classes from './weatherToday.module.scss'
import {useSelector} from "react-redux"
import moment from "moment";



const WeatherToday = () => {
    let weather = {
        "Clear": 'https://i.ibb.co/ZfkF23t/clear-1.png',
        "Clouds": 'https://i.ibb.co/Mf1s79H/clouds-2.png',
        "Rain": 'https://i.ibb.co/kQKN7xw/free-icon-rain-8722103.png',
        "Drizzle": "https://i.ibb.co/vjc3WbT/4837678.png",
        "Snow": "https://i.ibb.co/R2L5bbr/1247782.png"
    }
    const data = useSelector(state => state.data.data)

    return (
        <div className={classes.weatherToday}>
            {data && <div className={classes.temp}>
                <h2>{Math.floor(data.main.temp - 273, 2)}°C</h2>
                <p>Fells like: <span> {Math.floor(data.main.feels_like - 273, 2)}°C</span></p>
                <div className={classes.sun}>
                    <img
                        src="https://i.ibb.co/dBC09r7/sunrise-white-1.png"
                        alt=""/>
                    <div className={classes.text}>
                        <p>Sunrise</p>
                        <p className={classes.time}>{moment(data.sys.sunrise * 1000).utcOffset(data.timezone / 3600).format("HH:mm A")}</p>
                    </div>
                </div>
                <div className={classes.sun}>
                    <img
                        src="https://i.ibb.co/mXv8nsz/sunset-white-1.png"
                        alt=""/>
                    <div className={classes.text}>
                        <p>Sunset</p>
                        <p className={classes.time}>{moment(data.sys.sunset * 1000).utcOffset(data.timezone / 3600).format("HH:mm A")}</p>
                    </div>
                </div>
            </div>}
            <div className={classes.weather}>
                <img src={data && weather[data.weather[0].main]} alt=""/>
                <h2>{data && data.weather[0].main}</h2>
            </div>
            <div className={classes.info}>
                <div>
                    <img src="https://i.ibb.co/0myxRdZ/humidity-1.png" alt=""/>
                    <h4>{data && data.main.humidity + "%"}</h4>
                    <p>Humidity</p>
                </div>
                <div>
                    <img src="https://i.ibb.co/ccbwkCD/wind-1.png" alt=""/>
                    <h4>{data && data.wind.speed.toFixed(1)+ "m/s"}</h4>
                    <p>Wind Speed</p>
                </div>
                <div>
                    <img src="https://i.ibb.co/fXMtX2t/pressure-white-1.png" alt=""/>
                    <h4>{data && data.main.pressure + "hPa"}</h4>
                    <p>Pressure</p>
                </div>
                <div>
                    <img src="https://cdn-icons-png.flaticon.com/512/4225/4225597.png" alt=""/>
                    <h4>{data && data.visibility + "m"}</h4>
                    <p>Visibility</p>
                </div>
            </div>
        </div>
    )
        ;
};

export default WeatherToday;
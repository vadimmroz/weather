import classes from './weatherFiveDays.module.scss'
import {useSelector} from "react-redux";
import {useQuery} from "react-query";
import axios from "axios";
import moment from "moment";

const fetchForecast = async (city) => {
    if (city.lat) {
        return await axios.get(`https://api.weatherapi.com/v1/forecast.json?key=7cd703be21bf4d618f6182750230411&q=${city.lat},${city.lon}&days=5&aqi=yes&alerts=yes`)
    } else {
        return ''
    }
}

const WeatherFiveDays = () => {
    const city = useSelector(state => state.city.city)
    const forecast = useQuery(['forecast', city], () => fetchForecast(city), {
        keepPreviousData: true,
        refetchOnWindowFocus: false
    })

    return (
        <div className={classes.weatherFiveDays}>
            <h3>5 Days Forecast:</h3>
            <div className={classes.forecast}>
                {forecast.data && forecast.data.data.forecast.forecastday.map((e, index) => {
                    return (
                        <div key={index} className={classes.item}>
                            <img src={e.day.condition.icon} alt=""/>
                            <p>{Math.floor(e.day.maxtemp_c)}°C</p>
                            <p>{moment(e.date_epoch * 1000).format("dddd")}
                                <span>{moment(e.date_epoch * 1000).format("D MMM")}</span></p>
                        </div>)
                })}
            </div>
        </div>
    );
};

export default WeatherFiveDays;
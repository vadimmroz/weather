import classes from './weatherTimes.module.scss'
import axios from "axios";
import {useQuery} from "react-query";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {setPreloader} from "../../../../store/slices/preloaderSlice.js";
const fetchForecast = async (city) => {
    if (city.lat) {
        return await axios.get(`https://api.weatherapi.com/v1/forecast.json?key=7cd703be21bf4d618f6182750230411&q=${city.lat},${city.lon}&hour=5+6&aqi=yes&alerts=yes`)
    } else {
        return ''
    }
}

const WeatherTimes = () => {
    const city = useSelector(state => state.city.city)
    const {data, isLoading} = useQuery(['forecastHourly', city], ()=> fetchForecast(city))
    const dispatch = useDispatch()
    const theme = useSelector(state => state.theme.theme)
    useEffect(()=>{
        dispatch(setPreloader(isLoading))
    }, [data, isLoading])
    return (
        <div className={classes.weatherTimes}>
            <h3>Hourly Forecast:</h3>
            <div className={classes.container}>
                {data && data.data.forecast.forecastday[0].hour.map((e, index)=>{
                    let hour = [11,14,17,20,23]
                    if(index > 8 && index % 3 === 0 && hour.indexOf(index) !== 1){
                        return(
                            <div className={(index < 18 && theme === "light") ? classes.day : ""} key={index}>
                                <h3>{index} : 00</h3>
                                <img src={e.condition.icon} alt=""/>
                                <p>{Math.floor(e.temp_c)}°C</p>
                                <img style={{transform: `rotate(${e.wind_degree}deg)`}} src="https://i.ibb.co/Dz9chFt/navigation-1.png" alt="wind"/>
                                <p>{(e.wind_kph / 3.6).toFixed(1)}m/s</p>
                            </div>
                        )
                    }
                })}
            </div>
        </div>
    );
};

export default WeatherTimes;
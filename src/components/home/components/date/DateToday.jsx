import classes from './dateToday.module.scss'
import {useSelector} from "react-redux";
import {useEffect, useState} from "react";
import moment from "moment";

const DateToday = () => {
    const data = useSelector(state => state.data.data)


    const [date, setDate] = useState(moment())
    const [timezone, setTimeZone] = useState(data ? data.timezone / 3600 : 0)

    useEffect(() => {
        setTimeZone(data ? (data.timezone / 3600) : 0)
    }, [data]);

    const [intervals, setIntervals] = useState(null)

    useEffect(() => {
        const func = () => {
            let newDate = moment().utcOffset(timezone)
            if (newDate !== date) {
                setDate(moment().utcOffset(timezone))
            }
        }
        {
            intervals && clearInterval(intervals)
        }
        setIntervals(setInterval(func, 1000))

        //react-hooks/exhaustive-deps
    }, [timezone]);


    return (
        <div className={classes.dateToday}>
            {data &&
                <>
                    <h2>{data.name}</h2>
                    <h1>{date && date.format("HH:mm")}</h1>
                    <p>{date && date.format("LL")}</p>
                </>
            }
        </div>
    );
};

export default DateToday;
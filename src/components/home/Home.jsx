import classes from './home.module.scss'
import DateToday from "./components/date/DateToday.jsx";
import WeatherToday from "./components/weatherToday/WeatherToday.jsx";
import WeatherFiveDays from "./components/weatherFiveDays/WeatherFiveDays.jsx";
import WeatherTimes from "./components/weatherTimes/WeatherTimes.jsx";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {useQuery} from "react-query";
import {useEffect} from "react";
import {setData} from "../../store/slices/dataSlice.js";
import {setPreloader} from "../../store/slices/preloaderSlice.js";

const fetchData = async (city)=>{
    const {lat, lon} = city
    if(lat) {
        return await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=4d381ca2ba2085babc8c19011e7179c1`)
    }
    else{
        return undefined
    }
}

const Home = () => {
    const city = useSelector(state => state.city.city)
    const {data, isLoading} = useQuery(['data', city], ()=> fetchData(city), {
        keepPreviousData: true, refetchOnWindowFocus: false
    })
    const dispatch = useDispatch()
    useEffect(()=>{
        if(data){
            dispatch(setData(data.data))
        }
        dispatch(setPreloader(isLoading))
        //react-hooks/exhaustive-deps
    }, [data, dispatch, isLoading])

    return (
        <div className={classes.home}>
            <div className={classes.top}>
                <DateToday/>
                <WeatherToday/>
            </div>
            <div className={classes.bottom}>
                <WeatherFiveDays/>
                <WeatherTimes/>
            </div>
        </div>
    );
};

export default Home;
import './App.css'
import Header from "./components/header/Header.jsx";
import Home from "./components/home/Home.jsx";
import Preloader from "./components/preloader/Preloader.jsx";
import {useSelector} from "react-redux";


function App() {
    const preloader = useSelector(state => state.preloader.preloader)
    return (
        <>
            {preloader && <Preloader/>}
            <Header/>
            <Home/>
        </>
    )
}

export default App

import {createSlice} from "@reduxjs/toolkit";

const citySlice = createSlice({
    name: 'city',
    initialState: {
        city: {}
    },
    reducers: {
        setCity(state, action) {
            const {latitude, longitude} = action.payload
            state.city = {lat: latitude, lon: longitude}
        }
    }
})

export const {setCity} = citySlice.actions
export default citySlice.reducer
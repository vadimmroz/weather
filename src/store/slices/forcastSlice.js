import {createSlice} from "@reduxjs/toolkit";
const forcastSlice = createSlice({
    name:'forecast',
    initialState:{
        forecast: null
    },
    reducers:{
        setForecast(state, action){
            state.forecast = action.payload
        }
    }
})

export const {setForecast} = forcastSlice.actions

export default forcastSlice.reducer
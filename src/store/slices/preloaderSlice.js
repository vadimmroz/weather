import {createSlice} from "@reduxjs/toolkit";

const preloaderSlice = createSlice({
    name: 'preloader',
    initialState:{
        preloader: false
    },
    reducers:{
        setPreloader(state, action){
            state.preloader = action.payload
        }
    }
})

export const {setPreloader} = preloaderSlice.actions

export default preloaderSlice.reducer
import {configureStore} from "@reduxjs/toolkit";
import citySlice from "./slices/citySlice.js";
import dataSlice from "./slices/dataSlice.js";
import forcastSlice from "./slices/forcastSlice.js";
import preloaderSlice from "./slices/preloaderSlice.js";
import themeSlice from "./slices/themeSlice.js";
export default configureStore({
    reducer:{
        city: citySlice,
        data: dataSlice,
        forecast: forcastSlice,
        preloader: preloaderSlice,
        theme: themeSlice
    }
})
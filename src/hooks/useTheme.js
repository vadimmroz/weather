import {useLayoutEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {setThemeState} from "../store/slices/themeSlice.js";

export const useTheme = ()=>{
    const dispatch = useDispatch()
    const isDarkTheme = window?.matchMedia('(prefers-color-scheme: dark').matches
    const defaultTheme = isDarkTheme ? 'dark' : 'light'
    const [theme, setTheme] = useState(localStorage.getItem('app-theme') || defaultTheme)
    useLayoutEffect(()=>{
        document.documentElement.setAttribute('data-theme', theme)
        localStorage.setItem('app-theme', theme)
        dispatch(setThemeState(theme))
    }, [theme])

    return {theme, setTheme}
}
